﻿namespace VTe_Personel_veritabanı
{
    partial class Frmistatistik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbltoplampersonel = new System.Windows.Forms.Label();
            this.lblevlipersonel = new System.Windows.Forms.Label();
            this.lblbekarpersonel = new System.Windows.Forms.Label();
            this.lblsehirpersonel = new System.Windows.Forms.Label();
            this.lbltoplammaas = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblortalamamaas = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Toplam Personel Sayısı :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(90, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Evli Personel Sayısı :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(79, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Bekar Personel Sayısı :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(193, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Farklı Şehirlerden Olan Personel Sayısı :";
            // 
            // lbltoplampersonel
            // 
            this.lbltoplampersonel.AutoSize = true;
            this.lbltoplampersonel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbltoplampersonel.Location = new System.Drawing.Point(200, 9);
            this.lbltoplampersonel.Name = "lbltoplampersonel";
            this.lbltoplampersonel.Size = new System.Drawing.Size(16, 14);
            this.lbltoplampersonel.TabIndex = 4;
            this.lbltoplampersonel.Text = "0";
            // 
            // lblevlipersonel
            // 
            this.lblevlipersonel.AutoSize = true;
            this.lblevlipersonel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblevlipersonel.Location = new System.Drawing.Point(200, 36);
            this.lblevlipersonel.Name = "lblevlipersonel";
            this.lblevlipersonel.Size = new System.Drawing.Size(16, 14);
            this.lblevlipersonel.TabIndex = 5;
            this.lblevlipersonel.Text = "0";
            // 
            // lblbekarpersonel
            // 
            this.lblbekarpersonel.AutoSize = true;
            this.lblbekarpersonel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblbekarpersonel.Location = new System.Drawing.Point(200, 63);
            this.lblbekarpersonel.Name = "lblbekarpersonel";
            this.lblbekarpersonel.Size = new System.Drawing.Size(16, 14);
            this.lblbekarpersonel.TabIndex = 6;
            this.lblbekarpersonel.Text = "0";
            // 
            // lblsehirpersonel
            // 
            this.lblsehirpersonel.AutoSize = true;
            this.lblsehirpersonel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblsehirpersonel.Location = new System.Drawing.Point(200, 90);
            this.lblsehirpersonel.Name = "lblsehirpersonel";
            this.lblsehirpersonel.Size = new System.Drawing.Size(16, 14);
            this.lblsehirpersonel.TabIndex = 7;
            this.lblsehirpersonel.Text = "0";
            // 
            // lbltoplammaas
            // 
            this.lbltoplammaas.AutoSize = true;
            this.lbltoplammaas.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbltoplammaas.Location = new System.Drawing.Point(200, 117);
            this.lbltoplammaas.Name = "lbltoplammaas";
            this.lbltoplammaas.Size = new System.Drawing.Size(16, 14);
            this.lbltoplammaas.TabIndex = 9;
            this.lbltoplammaas.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(117, 117);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Toplam Maaş :";
            // 
            // lblortalamamaas
            // 
            this.lblortalamamaas.AutoSize = true;
            this.lblortalamamaas.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblortalamamaas.Location = new System.Drawing.Point(200, 144);
            this.lblortalamamaas.Name = "lblortalamamaas";
            this.lblortalamamaas.Size = new System.Drawing.Size(16, 14);
            this.lblortalamamaas.TabIndex = 11;
            this.lblortalamamaas.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(110, 144);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Ortalama Maaş :";
            // 
            // Frmistatistik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 173);
            this.Controls.Add(this.lblortalamamaas);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lbltoplammaas);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblsehirpersonel);
            this.Controls.Add(this.lblbekarpersonel);
            this.Controls.Add(this.lblevlipersonel);
            this.Controls.Add(this.lbltoplampersonel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Frmistatistik";
            this.Text = "Frmistatistik";
            this.Load += new System.EventHandler(this.Frmistatistik_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbltoplampersonel;
        private System.Windows.Forms.Label lblevlipersonel;
        private System.Windows.Forms.Label lblbekarpersonel;
        private System.Windows.Forms.Label lblsehirpersonel;
        private System.Windows.Forms.Label lbltoplammaas;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblortalamamaas;
        private System.Windows.Forms.Label label12;
    }
}