﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace VTe_Personel_veritabanı
{
    public partial class GrafikForm : Form
    {
        public GrafikForm()
        {
            InitializeComponent();
        }
        SqlConnection baglantiii = new SqlConnection(@"Data Source=DESKTOP-PP0L2HT;Initial Catalog=PersonelVTe;Integrated Security=True");

        private void GrafikForm_Load(object sender, EventArgs e)
        {
            baglantiii.Open();
            SqlCommand komutg1 = new SqlCommand("Select PersonelSehir, Count(*) From VTe_Personel Group By PersonelSehir", baglantiii);
            SqlDataReader drs1 = komutg1.ExecuteReader();
            while (drs1.Read())
            {
                chart1.Series["Sehirler"].Points.AddXY(drs1[0], drs1[1]);
            }


            baglantiii.Close();
        }
    }
}
