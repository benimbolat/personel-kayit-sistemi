﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace VTe_Personel_veritabanı
{
    public partial class FormGirisPanel : Form
    {
        public FormGirisPanel()
        {
            InitializeComponent();
        }
        SqlConnection baglanti = new SqlConnection(@"Data Source=DESKTOP-PP0L2HT;Initial Catalog=PersonelVTe;Integrated Security=True");

        private void button1_Click(object sender, EventArgs e)
        {
            baglanti.Open();
            SqlCommand kmt = new SqlCommand("Select * From VTe_Yönetici Where KullaniciAdi=@p1 and Sifre=@p2",baglanti);
            kmt.Parameters.AddWithValue("@p1", textBox1.Text);
            kmt.Parameters.AddWithValue("@p2", textBox2.Text);
            SqlDataReader dr = kmt.ExecuteReader();
            if (dr.Read())
            {
                AnaForm frm = new AnaForm();
                frm.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Kullanıcı Adınız veya Şifreniz Hatalı", "Hatalı Giriş", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
            baglanti.Close();

        }
    }
}
