﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace VTe_Personel_veritabanı
{
    public partial class Frmistatistik : Form
    {
        SqlConnection baglantii = new SqlConnection(@"Data Source=DESKTOP-PP0L2HT;Initial Catalog=PersonelVTe;Integrated Security=True");

        public Frmistatistik()
        {
            InitializeComponent();
        }

        private void Frmistatistik_Load(object sender, EventArgs e)
        {
            //Toplam Personel Sayısı
            baglantii.Open();
            SqlCommand komut1 = new SqlCommand("select count (*) from VTe_Personel", baglantii);
            SqlDataReader dr1 = komut1.ExecuteReader();           // Select için Veri okuyucu tanımlanıyor 
            while (dr1.Read())
            {
                lbltoplampersonel.Text = dr1[0].ToString();
            }
          
            baglantii.Close();


            //Evli Personel Sayısı
            baglantii.Open();
            SqlCommand komut2 = new SqlCommand("Select count (*) From VTe_Personel where PersonelDurum=1", baglantii);
            SqlDataReader dr2 = komut2.ExecuteReader();
            while (dr2.Read())
            {
                lblevlipersonel.Text = dr2[0].ToString();
            }

            baglantii.Close();

            //Bekar Personel Sayısı
            baglantii.Open();
            SqlCommand komut3 = new SqlCommand("Select count (*) From VTe_Personel where PersonelDurum=0", baglantii);
            SqlDataReader dr3 = komut3.ExecuteReader();
            while (dr3.Read())
            {
                lblbekarpersonel.Text = dr3[0].ToString();
            }

            baglantii.Close();

            //Farklı Sehir Personel Sayısı
            baglantii.Open();
            SqlCommand komut4 = new SqlCommand("Select Count(distinct(PersonelSehir)) From VTe_Personel", baglantii);
            SqlDataReader dr4 = komut4.ExecuteReader();
            while (dr4.Read())
            {
                lblsehirpersonel.Text = dr4[0].ToString();
            }

            baglantii.Close();

            //Personel Toplam Maaş
            baglantii.Open();
            SqlCommand komut5 = new SqlCommand("Select Sum(PersonelMaas) From VTe_Personel", baglantii);
            SqlDataReader dr5 = komut5.ExecuteReader();
            while (dr5.Read())
            {
                lbltoplammaas.Text = dr5[0].ToString();
            }

            baglantii.Close();

            //Personel Toplam Maaş
            baglantii.Open();
            SqlCommand komut6 = new SqlCommand("Select Avg(PersonelMaas) From VTe_Personel", baglantii);
            SqlDataReader dr6 = komut6.ExecuteReader();
            while (dr6.Read())
            {
                lblortalamamaas.Text = dr6[0].ToString();
            }

            baglantii.Close();

        }
    }
}
