﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace VTe_Personel_veritabanı
{
    public partial class AnaForm : Form
    {
        public AnaForm()
        {
            InitializeComponent();
        }

        SqlConnection baglanti = new SqlConnection(@"Data Source=DESKTOP-PP0L2HT;Initial Catalog=PersonelVTe;Integrated Security=True");

        void temizle()
        {
            tbid.Text = "";
            tbad.Text = "";
            tbsoyad.Text = "";
            tbsehir.Text = "";
            tbmeslek.Text = "";
            maskMaas.Text = "";
            radioBekar.Checked = false;
            radioEVLi.Checked = false;
            tbad.Focus();
        }

        void listele()
        {
            this.vTe_PersonelTableAdapter.Fill(this.personelVTeDataSet2.VTe_Personel);
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'personelVTeDataSet2.VTe_Personel' table. You can move, or remove it, as needed.
            listele();
        }
        private void radioBekar_CheckedChanged(object sender, EventArgs e)
        {
            if (radioBekar.Checked == true)
            {
                label9.Text = "False";

            }
        }

        private void radioEVLi_CheckedChanged(object sender, EventArgs e)
        {
            if (radioEVLi.Checked == true)
            {
                label9.Text = "True";
            }
        }
        private void label9_Click(object sender, EventArgs e)
        {
            if (label9.Text == "True")
            {
                radioBekar.Checked = false;

                radioEVLi.Checked = true;
             
            }
            if (label9.Text == "False")
            {
                radioEVLi.Checked = false;

                radioBekar.Checked = true;
              
            }
        }


        private void bttnTemizle_Click(object sender, EventArgs e)
        {
            temizle();
        }

        private void bttnListele_Click(object sender, EventArgs e)
        {
            this.vTe_PersonelTableAdapter.Fill(this.personelVTeDataSet2.VTe_Personel);

        }

        private void bttnKaydet_Click(object sender, EventArgs e)
        {
            baglanti.Open();
            SqlCommand komut = new SqlCommand("insert into VTe_Personel (PersonelAd,PersonelSAd,PersonelSehir,PersonelMaas,PersonelMeslek) values (@p1,@p2,@p3,@p4,@p5)", baglanti);

            komut.Parameters.AddWithValue("@p1", tbad.Text);
            komut.Parameters.AddWithValue("@p2", tbsoyad.Text);
            komut.Parameters.AddWithValue("@p3", tbsehir.Text);
            komut.Parameters.AddWithValue("@p4", maskMaas.Text);
            komut.Parameters.AddWithValue("@p5", tbmeslek.Text);

            //komut.ExecuteNonQuery();

            baglanti.Close();
            MessageBox.Show("Personel eklendi.");
            listele();
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int secilendeger = dataGridView2.SelectedCells[0].RowIndex;

            tbid.Text = dataGridView2.Rows[secilendeger].Cells[0].Value.ToString();
            tbad.Text = dataGridView2.Rows[secilendeger].Cells[1].Value.ToString();
            tbsoyad.Text = dataGridView2.Rows[secilendeger].Cells[2].Value.ToString();
            tbsehir.Text = dataGridView2.Rows[secilendeger].Cells[3].Value.ToString();
            maskMaas.Text = dataGridView2.Rows[secilendeger].Cells[4].Value.ToString();
            label9.Text = dataGridView2.Rows[secilendeger].Cells[5].Value.ToString();
            tbmeslek.Text = dataGridView2.Rows[secilendeger].Cells[6].Value.ToString();

        }

        private void bttnSil_Click(object sender, EventArgs e)
        {
            baglanti.Open();
            SqlCommand komutsil = new SqlCommand("Delete From VTe_Personel where PersonelID=@p1", baglanti);
            komutsil.Parameters.AddWithValue("@p1", tbid.Text);
            komutsil.ExecuteNonQuery();
            baglanti.Close();

            MessageBox.Show("Kitap Veri Tabanından Silindi.", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            listele();
        }

        private void bttnGuncelle_Click(object sender, EventArgs e)
        {
            baglanti.Open();
            SqlCommand komutguncelle = new SqlCommand("Update VTe_Personel set PersonelAd=@p1,PersonelSAd=@p2,PersonelSehir=@p3,PersonelMaas=@p4,PersonelMeslek=@p5,PersonelDurum=@p6 where PersonelID=@p7", baglanti);
            komutguncelle.Parameters.AddWithValue("@p1", tbad.Text);
            komutguncelle.Parameters.AddWithValue("@p2", tbsoyad.Text);
            komutguncelle.Parameters.AddWithValue("@p3", tbsehir.Text);
            komutguncelle.Parameters.AddWithValue("@p4", maskMaas.Text);
            komutguncelle.Parameters.AddWithValue("@p5", tbmeslek.Text);
            komutguncelle.Parameters.AddWithValue("@p6", label9.Text);
            komutguncelle.Parameters.AddWithValue("@p7", tbid.Text);
            komutguncelle.ExecuteNonQuery();
            baglanti.Close();
            listele();
            MessageBox.Show("Personel Bilgileri Güncellendi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

        private void bttnİstatistik_Click(object sender, EventArgs e)
        {
            Frmistatistik fr = new Frmistatistik();
            fr.Show();
        }

        private void bttnGrafikler_Click(object sender, EventArgs e)
        {
            GrafikForm frg = new GrafikForm();
            frg.Show();
        }

     
    }
}
