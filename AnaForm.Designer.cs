﻿namespace VTe_Personel_veritabanı
{
    partial class AnaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnaForm));
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.personelIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personelAdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personelSAdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personelSehirDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personelMaasDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personelDurumDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.personelMeslekDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vTePersonelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.personelVTeDataSet2 = new VTe_Personel_veritabanı.PersonelVTeDataSet2();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bttnGrafikler = new System.Windows.Forms.Button();
            this.bttnİstatistik = new System.Windows.Forms.Button();
            this.bttnTemizle = new System.Windows.Forms.Button();
            this.bttnSil = new System.Windows.Forms.Button();
            this.bttnGuncelle = new System.Windows.Forms.Button();
            this.bttnKaydet = new System.Windows.Forms.Button();
            this.bttnListele = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioBekar = new System.Windows.Forms.RadioButton();
            this.radioEVLi = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.tbmeslek = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.maskMaas = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbsehir = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbsoyad = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbad = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.vTe_PersonelTableAdapter = new VTe_Personel_veritabanı.PersonelVTeDataSet2TableAdapters.VTe_PersonelTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vTePersonelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personelVTeDataSet2)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(198, 190);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Durumu :";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(505, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(240, 239);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridView2);
            this.groupBox3.Location = new System.Drawing.Point(2, 243);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(749, 201);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Personel Bilgi";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.personelIDDataGridViewTextBoxColumn,
            this.personelAdDataGridViewTextBoxColumn,
            this.personelSAdDataGridViewTextBoxColumn,
            this.personelSehirDataGridViewTextBoxColumn,
            this.personelMaasDataGridViewTextBoxColumn,
            this.personelDurumDataGridViewCheckBoxColumn,
            this.personelMeslekDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.vTePersonelBindingSource;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 16);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(743, 182);
            this.dataGridView2.TabIndex = 1;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            this.dataGridView2.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellDoubleClick);
            // 
            // personelIDDataGridViewTextBoxColumn
            // 
            this.personelIDDataGridViewTextBoxColumn.DataPropertyName = "PersonelID";
            this.personelIDDataGridViewTextBoxColumn.HeaderText = "PersonelID";
            this.personelIDDataGridViewTextBoxColumn.Name = "personelIDDataGridViewTextBoxColumn";
            this.personelIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // personelAdDataGridViewTextBoxColumn
            // 
            this.personelAdDataGridViewTextBoxColumn.DataPropertyName = "PersonelAd";
            this.personelAdDataGridViewTextBoxColumn.HeaderText = "PersonelAd";
            this.personelAdDataGridViewTextBoxColumn.Name = "personelAdDataGridViewTextBoxColumn";
            // 
            // personelSAdDataGridViewTextBoxColumn
            // 
            this.personelSAdDataGridViewTextBoxColumn.DataPropertyName = "PersonelSAd";
            this.personelSAdDataGridViewTextBoxColumn.HeaderText = "PersonelSAd";
            this.personelSAdDataGridViewTextBoxColumn.Name = "personelSAdDataGridViewTextBoxColumn";
            // 
            // personelSehirDataGridViewTextBoxColumn
            // 
            this.personelSehirDataGridViewTextBoxColumn.DataPropertyName = "PersonelSehir";
            this.personelSehirDataGridViewTextBoxColumn.HeaderText = "PersonelSehir";
            this.personelSehirDataGridViewTextBoxColumn.Name = "personelSehirDataGridViewTextBoxColumn";
            // 
            // personelMaasDataGridViewTextBoxColumn
            // 
            this.personelMaasDataGridViewTextBoxColumn.DataPropertyName = "PersonelMaas";
            this.personelMaasDataGridViewTextBoxColumn.HeaderText = "PersonelMaas";
            this.personelMaasDataGridViewTextBoxColumn.Name = "personelMaasDataGridViewTextBoxColumn";
            // 
            // personelDurumDataGridViewCheckBoxColumn
            // 
            this.personelDurumDataGridViewCheckBoxColumn.DataPropertyName = "PersonelDurum";
            this.personelDurumDataGridViewCheckBoxColumn.HeaderText = "PersonelDurum";
            this.personelDurumDataGridViewCheckBoxColumn.Name = "personelDurumDataGridViewCheckBoxColumn";
            // 
            // personelMeslekDataGridViewTextBoxColumn
            // 
            this.personelMeslekDataGridViewTextBoxColumn.DataPropertyName = "PersonelMeslek";
            this.personelMeslekDataGridViewTextBoxColumn.HeaderText = "PersonelMeslek";
            this.personelMeslekDataGridViewTextBoxColumn.Name = "personelMeslekDataGridViewTextBoxColumn";
            // 
            // vTePersonelBindingSource
            // 
            this.vTePersonelBindingSource.DataMember = "VTe_Personel";
            this.vTePersonelBindingSource.DataSource = this.personelVTeDataSet2;
            // 
            // personelVTeDataSet2
            // 
            this.personelVTeDataSet2.DataSetName = "PersonelVTeDataSet2";
            this.personelVTeDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bttnGrafikler);
            this.groupBox2.Controls.Add(this.bttnİstatistik);
            this.groupBox2.Controls.Add(this.bttnTemizle);
            this.groupBox2.Controls.Add(this.bttnSil);
            this.groupBox2.Controls.Add(this.bttnGuncelle);
            this.groupBox2.Controls.Add(this.bttnKaydet);
            this.groupBox2.Controls.Add(this.bttnListele);
            this.groupBox2.Location = new System.Drawing.Point(276, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(223, 225);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Seçenekler";
            // 
            // bttnGrafikler
            // 
            this.bttnGrafikler.Location = new System.Drawing.Point(117, 33);
            this.bttnGrafikler.Name = "bttnGrafikler";
            this.bttnGrafikler.Size = new System.Drawing.Size(100, 23);
            this.bttnGrafikler.TabIndex = 6;
            this.bttnGrafikler.Text = "Grafikler";
            this.bttnGrafikler.UseVisualStyleBackColor = true;
            this.bttnGrafikler.Click += new System.EventHandler(this.bttnGrafikler_Click);
            // 
            // bttnİstatistik
            // 
            this.bttnİstatistik.Location = new System.Drawing.Point(6, 123);
            this.bttnİstatistik.Name = "bttnİstatistik";
            this.bttnİstatistik.Size = new System.Drawing.Size(100, 23);
            this.bttnİstatistik.TabIndex = 5;
            this.bttnİstatistik.Text = "İstatistik";
            this.bttnİstatistik.UseVisualStyleBackColor = true;
            this.bttnİstatistik.Click += new System.EventHandler(this.bttnİstatistik_Click);
            // 
            // bttnTemizle
            // 
            this.bttnTemizle.Location = new System.Drawing.Point(117, 123);
            this.bttnTemizle.Name = "bttnTemizle";
            this.bttnTemizle.Size = new System.Drawing.Size(100, 23);
            this.bttnTemizle.TabIndex = 4;
            this.bttnTemizle.Text = "Temizle";
            this.bttnTemizle.UseVisualStyleBackColor = true;
            this.bttnTemizle.Click += new System.EventHandler(this.bttnTemizle_Click);
            // 
            // bttnSil
            // 
            this.bttnSil.Location = new System.Drawing.Point(117, 78);
            this.bttnSil.Name = "bttnSil";
            this.bttnSil.Size = new System.Drawing.Size(100, 23);
            this.bttnSil.TabIndex = 3;
            this.bttnSil.Text = "Sil";
            this.bttnSil.UseVisualStyleBackColor = true;
            this.bttnSil.Click += new System.EventHandler(this.bttnSil_Click);
            // 
            // bttnGuncelle
            // 
            this.bttnGuncelle.Location = new System.Drawing.Point(6, 78);
            this.bttnGuncelle.Name = "bttnGuncelle";
            this.bttnGuncelle.Size = new System.Drawing.Size(100, 23);
            this.bttnGuncelle.TabIndex = 2;
            this.bttnGuncelle.Text = "Güncelle";
            this.bttnGuncelle.UseVisualStyleBackColor = true;
            this.bttnGuncelle.Click += new System.EventHandler(this.bttnGuncelle_Click);
            // 
            // bttnKaydet
            // 
            this.bttnKaydet.Location = new System.Drawing.Point(6, 166);
            this.bttnKaydet.Name = "bttnKaydet";
            this.bttnKaydet.Size = new System.Drawing.Size(211, 37);
            this.bttnKaydet.TabIndex = 1;
            this.bttnKaydet.Text = "Kaydet";
            this.bttnKaydet.UseVisualStyleBackColor = true;
            this.bttnKaydet.Click += new System.EventHandler(this.bttnKaydet_Click);
            // 
            // bttnListele
            // 
            this.bttnListele.Location = new System.Drawing.Point(6, 33);
            this.bttnListele.Name = "bttnListele";
            this.bttnListele.Size = new System.Drawing.Size(100, 23);
            this.bttnListele.TabIndex = 0;
            this.bttnListele.Text = "Listele";
            this.bttnListele.UseVisualStyleBackColor = true;
            this.bttnListele.Click += new System.EventHandler(this.bttnListele_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.radioBekar);
            this.groupBox1.Controls.Add(this.radioEVLi);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tbmeslek);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.maskMaas);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbsehir);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbsoyad);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbad);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbid);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(1, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(269, 225);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Personel Kayıt";
            // 
            // radioBekar
            // 
            this.radioBekar.AutoSize = true;
            this.radioBekar.Location = new System.Drawing.Point(139, 189);
            this.radioBekar.Name = "radioBekar";
            this.radioBekar.Size = new System.Drawing.Size(53, 17);
            this.radioBekar.TabIndex = 8;
            this.radioBekar.TabStop = true;
            this.radioBekar.Text = "Bekar";
            this.radioBekar.UseVisualStyleBackColor = true;
            this.radioBekar.CheckedChanged += new System.EventHandler(this.radioBekar_CheckedChanged);
            // 
            // radioEVLi
            // 
            this.radioEVLi.AutoSize = true;
            this.radioEVLi.Location = new System.Drawing.Point(91, 189);
            this.radioEVLi.Name = "radioEVLi";
            this.radioEVLi.Size = new System.Drawing.Size(42, 17);
            this.radioEVLi.TabIndex = 7;
            this.radioEVLi.TabStop = true;
            this.radioEVLi.Text = "Evli";
            this.radioEVLi.UseVisualStyleBackColor = true;
            this.radioEVLi.CheckedChanged += new System.EventHandler(this.radioEVLi_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Durumu :";
            // 
            // tbmeslek
            // 
            this.tbmeslek.Location = new System.Drawing.Point(91, 163);
            this.tbmeslek.Name = "tbmeslek";
            this.tbmeslek.Size = new System.Drawing.Size(100, 20);
            this.tbmeslek.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Mesleği :";
            // 
            // maskMaas
            // 
            this.maskMaas.Location = new System.Drawing.Point(91, 137);
            this.maskMaas.Mask = "00000";
            this.maskMaas.Name = "maskMaas";
            this.maskMaas.Size = new System.Drawing.Size(100, 20);
            this.maskMaas.TabIndex = 5;
            this.maskMaas.ValidatingType = typeof(int);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Maaşı :";
            // 
            // tbsehir
            // 
            this.tbsehir.Location = new System.Drawing.Point(91, 108);
            this.tbsehir.Name = "tbsehir";
            this.tbsehir.Size = new System.Drawing.Size(100, 20);
            this.tbsehir.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Şehir :";
            // 
            // tbsoyad
            // 
            this.tbsoyad.Location = new System.Drawing.Point(91, 82);
            this.tbsoyad.Name = "tbsoyad";
            this.tbsoyad.Size = new System.Drawing.Size(100, 20);
            this.tbsoyad.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Soyadı :";
            // 
            // tbad
            // 
            this.tbad.Location = new System.Drawing.Point(91, 56);
            this.tbad.Name = "tbad";
            this.tbad.Size = new System.Drawing.Size(100, 20);
            this.tbad.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Adı :";
            // 
            // tbid
            // 
            this.tbid.Location = new System.Drawing.Point(91, 30);
            this.tbid.Name = "tbid";
            this.tbid.Size = new System.Drawing.Size(100, 20);
            this.tbid.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Personel ID :";
            // 
            // vTe_PersonelTableAdapter
            // 
            this.vTe_PersonelTableAdapter.ClearBeforeFill = true;
            // 
            // AnaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 442);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AnaForm";
            this.Text = "Personel Kayıt Sistemi / M. Enes Bolat";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vTePersonelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personelVTeDataSet2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bttnGrafikler;
        private System.Windows.Forms.Button bttnİstatistik;
        private System.Windows.Forms.Button bttnTemizle;
        private System.Windows.Forms.Button bttnSil;
        private System.Windows.Forms.Button bttnGuncelle;
        private System.Windows.Forms.Button bttnKaydet;
        private System.Windows.Forms.Button bttnListele;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioBekar;
        private System.Windows.Forms.RadioButton radioEVLi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbmeslek;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox maskMaas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbsehir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbsoyad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private PersonelVTeDataSet2 personelVTeDataSet2;
        private System.Windows.Forms.BindingSource vTePersonelBindingSource;
        private PersonelVTeDataSet2TableAdapters.VTe_PersonelTableAdapter vTe_PersonelTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn personelIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn personelAdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn personelSAdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn personelSehirDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn personelMaasDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn personelDurumDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn personelMeslekDataGridViewTextBoxColumn;
    }
}

